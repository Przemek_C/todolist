package com.example.rent.todolist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.rent.todo.TodoContract;

import java.util.ArrayList;

/**
 * Created by RENT on 2017-07-05.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
    SQLiteDatabase db;
    public static final int DB_VERSION = 3;
    public static final String DB_NAME = "TODO.db";

    public static final String CREATE_DB = "CREATE TABLE " + TodoContract.Todo.DB_TABLE_NAME
            + "(" + TodoContract.Todo._ID + " INTEGER PRIMARY KEY, " +
            TodoContract.Todo.DB_TASK_COLUMN + " TEXT)";

    public static final String DELETE_DB = "DROP TABLE IF EXISTS " + TodoContract.Todo.DB_TABLE_NAME;


    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DELETE_DB);
        onCreate(db);
    }

    public void createTask(String task){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TodoContract.Todo.DB_TASK_COLUMN, task);
        db.insertWithOnConflict(TodoContract.Todo.DB_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public void deleteTask(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = TodoContract.Todo._ID + " = ? ";
        String[] selectionArg = {id};
        db.delete(TodoContract.Todo.DB_TABLE_NAME, selection, selectionArg);
    }

    public ArrayList<String> getTaskList(){
        ArrayList<String> taskList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TodoContract.Todo.DB_TABLE_NAME, null, null, null, null, null, null);
        TodoModel todoModel;
        if(cursor.getCount() > 0){
            for(int i = 0; i < cursor.getCount(); i++){
                cursor.moveToNext();
                todoModel = new TodoModel();
                todoModel.setId(cursor.getInt(0));
                todoModel.setTaskName(cursor.getString(1));
                taskList.add(cursor.getString(1));
            }
        }
        return taskList;
    }
}
