package com.example.rent.todo;

import android.provider.BaseColumns;

/**
 * Created by RENT on 2017-07-05.
 */

public final class TodoContract {
    private TodoContract(){}

public static class Todo implements BaseColumns {
    public static final String DB_TABLE_NAME = "Task";
    public static final String DB_TASK_COLUMN = "Task_name";
    }
}

